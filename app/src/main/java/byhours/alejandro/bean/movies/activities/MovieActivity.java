package byhours.alejandro.bean.movies.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import byhours.alejandro.bean.movies.R;
import byhours.alejandro.bean.movies.adapters.MoviesAdapter;
import byhours.alejandro.bean.movies.adapters.SimilarAdapter;
import byhours.alejandro.bean.movies.interfaces.IMoviesHandler;
import byhours.alejandro.bean.movies.model.Movie;
import byhours.alejandro.bean.movies.singletons.DataSource;
import byhours.alejandro.bean.movies.util.RecyclerTouchListener;

public class MovieActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private SimilarAdapter mSimilarAdapter;
    private LinearLayoutManager mLayoutManager;

    TextView mTitle;

    ArrayList<Movie> movies = new ArrayList<Movie>();
    private String movieId;
    private Movie movieSelected = new Movie();



    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        mTitle = (TextView) findViewById(R.id.similar_title);
        getMovieValues();

        setupProgressDialog();

        setupMovies();
    }

    private void getMovieValues(){
        movieId = getIntent().getStringExtra("movieID");
        String movieTitle = getIntent().getStringExtra("movieTitle");
        movieSelected.setTitle(movieTitle);
        String movieImage = getIntent().getStringExtra("movieImage");
        movieSelected.setPoster_path(movieImage);
        String movieOverview = getIntent().getStringExtra("movieOverview");
        movieSelected.setOverview(movieOverview);
        String movieCount = getIntent().getStringExtra("movieCount");
        movieSelected.setVote_count(Integer.parseInt(movieCount));
        String movieAvg = getIntent().getStringExtra("movieAvg");
        movieSelected.setVote_average(Double.parseDouble(movieAvg));
        String moviePopularity = getIntent().getStringExtra("moviePopularity");
        movieSelected.setPopularity(Double.parseDouble(moviePopularity));
        String movieDate = getIntent().getStringExtra("movieDate");
        movieSelected.setRelease_date(movieDate);
        String movieOriginalLang = getIntent().getStringExtra("movieOriginalLang");
        movieSelected.setOriginal_language(movieOriginalLang);
        String movieOriginalTitle = getIntent().getStringExtra("movieOriginalTitle");
        movieSelected.setOriginal_title(movieOriginalTitle);

        mTitle.setText("Similar to: "+movieTitle);
        movies.add(movieSelected);
    }

    private void setupMovies(){
        mRecyclerView =  (RecyclerView) findViewById(R.id.similar_recycler_view);

//        movies.clear();
        dialog.show();

        DataSource.GetDataSourceObject(this).getSimilarMovies(movieId,new IMoviesHandler() {
            @Override
            public void OnGetMovies(List<Movie> list) {
                movies.addAll(list);
                mLayoutManager = new LinearLayoutManager(getBaseContext(), LinearLayoutManager.HORIZONTAL, false);
                mRecyclerView.setLayoutManager(mLayoutManager);

                mSimilarAdapter = new SimilarAdapter(getBaseContext(), movies );
                //mDetailsAdapter.notifyDataSetChanged();

                mRecyclerView.setAdapter(mSimilarAdapter);
                dialog.cancel();
            }

            @Override
            public void OnFailure(String message) {
//                displayError("Login failed!", "Impossible to access to the account");
                dialog.cancel();
            }
        });


    }

    private void setupProgressDialog() {
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Loading. Please wait...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
    }
}
