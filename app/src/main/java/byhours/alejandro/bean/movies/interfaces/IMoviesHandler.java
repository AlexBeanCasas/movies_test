package byhours.alejandro.bean.movies.interfaces;

import java.util.List;

import byhours.alejandro.bean.movies.model.Movie;

public interface IMoviesHandler extends IFailureCallback {
    void OnGetMovies(List<Movie> list);
}
