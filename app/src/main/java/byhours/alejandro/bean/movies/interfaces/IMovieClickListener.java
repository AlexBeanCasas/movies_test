package byhours.alejandro.bean.movies.interfaces;

import android.view.View;

public interface IMovieClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
