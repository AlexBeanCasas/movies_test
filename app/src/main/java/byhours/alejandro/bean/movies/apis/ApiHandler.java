package byhours.alejandro.bean.movies.apis;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import byhours.alejandro.bean.movies.interfaces.ApiCallback;

public class ApiHandler {

    private final Context currentContex;
    static ApiHandler handler;
    RequestQueue queue;
    String TAG = "ApiHandler";
    String BASE_URL = "http://api.themoviedb.org/3/movie/";

    //TODO: It should be secret in a private config file
    String API_KEY = "?api_key=9956757cfd1eee145bb9a96e91a165e7";

    public static ApiHandler GetObject(Context cntx) {
        if (handler == null)
            handler = new ApiHandler(cntx);
        return handler;
    }


    private ApiHandler(Context cntx) {
        this.currentContex = cntx;
        queue = Volley.newRequestQueue(cntx);
    }

    public void DoGet(String url, final ApiCallback callback) {

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.OnSuccess(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // to find no network connection error.
                        callback.OnFailure(ParseVolleyError(error));
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("X-Auth-Token", "fc8d4f2799f94e6187f8363cad28c759");
                return params;
            }
        };

        queue.add(stringRequest);
    }


    private String ParseVolleyError(VolleyError volleyError) {
        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }else
            message = volleyError.getMessage();

        return message;
    }

    public void getMovies(final ApiCallback callback) {
        if (callback == null)
            return;

        // Request a string response from the provided URL.
        DoGet(BASE_URL + "popular"+ API_KEY,
                new ApiCallback() {
                    @Override
                    public void OnSuccess(String responce) {
                        try {
                            callback.OnSuccess(responce);
                        } catch (Exception e) {
                            Log.i(TAG, e.getMessage());

                            callback.OnFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void OnFailure(String responce) {
                        callback.OnFailure(responce);
                    }
                });
    }

    public void getSimilarMovies(String id,final ApiCallback callback) {
        if (callback == null)
            return;

        // Request a string response from the provided URL.
        DoGet(BASE_URL + id+"/similar"+API_KEY,
                new ApiCallback() {
                    @Override
                    public void OnSuccess(String responce) {
                        try {
                            callback.OnSuccess(responce);
                        } catch (Exception e) {
                            Log.i(TAG, e.getMessage());

                            callback.OnFailure(e.getMessage());
                        }
                    }

                    @Override
                    public void OnFailure(String responce) {
                        callback.OnFailure(responce);
                    }
                });
    }
}
