package byhours.alejandro.bean.movies.interfaces;

public interface ApiCallback {
    void OnSuccess(String result);
    void OnFailure(String message);
}
