package byhours.alejandro.bean.movies.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import byhours.alejandro.bean.movies.R;
import byhours.alejandro.bean.movies.model.Movie;
import jp.wasabeef.picasso.transformations.CropSquareTransformation;
import jp.wasabeef.picasso.transformations.CropTransformation;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {

    private ArrayList<Movie> mDataset;
    private int currentPosition = 0;
    private LayoutInflater inflater;
    Context cntx;
    private final String IMG_URL = "https://image.tmdb.org/t/p/w500/";

    public MoviesAdapter(Context xntx, ArrayList<Movie> list) {
        inflater = LayoutInflater.from(xntx);
        cntx = xntx;
        mDataset = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView logoView;
        public TextView titleView;
        public TextView ratingView;

        public ViewHolder(View v) {
            super(v);
            logoView = (ImageView) v.findViewById(R.id.logo_movie);
            titleView = (TextView) v.findViewById(R.id.title_movie);
            ratingView = (TextView) v.findViewById(R.id.rating_movie);
        }
    }

    @Override
    public MoviesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.movie_item_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Movie movie = mDataset.get(position);
        holder.titleView.setText(movie.getTitle());
        holder.ratingView.setText(String.valueOf(movie.getVote_average()));
        Picasso.get().load(IMG_URL+movie.getBackdrop_path())
                .fit()
                .centerCrop()
                .into(holder.logoView);


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }




}
