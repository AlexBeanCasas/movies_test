package byhours.alejandro.bean.movies.parsers;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import byhours.alejandro.bean.movies.model.Movie;
import byhours.alejandro.bean.movies.model.QueryFormat;

public class ModelParser {
    static Gson gson = new Gson();

    public static List<Movie> getMovies(String result) {
        List<Movie> list = new ArrayList<>();
        try {
            QueryFormat response = gson.fromJson(result,QueryFormat.class);
            Movie [] movies = response.getResults();
            list = new ArrayList<>(Arrays.asList(movies));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return (list);
    }
}
