package byhours.alejandro.bean.movies.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import byhours.alejandro.bean.movies.R;
import byhours.alejandro.bean.movies.model.Movie;

public class SimilarAdapter extends RecyclerView.Adapter<SimilarAdapter.ViewHolder>  {
    private ArrayList<Movie> mDataset;
    private int currentPosition = 0;
    private LayoutInflater inflater;
    Context cntx;
    private final String IMG_URL = "https://image.tmdb.org/t/p/w500/";

    public SimilarAdapter(Context xntx, ArrayList<Movie> list) {
        inflater = LayoutInflater.from(xntx);
        cntx = xntx;
        mDataset = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView imageView;
        public TextView titleView;
        public TextView overviewView;
        public TextView popularityView;
        public TextView countView;
        public TextView avgView;
        public TextView dateView;
        public TextView originalLangView;
        public TextView originalTitleView;

        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.logo_selection);
            titleView = (TextView) v.findViewById(R.id.title_selection);
            overviewView = (TextView) v.findViewById(R.id.overview_selection);
            popularityView = (TextView) v.findViewById(R.id.popularity_selection);
            countView = (TextView) v.findViewById(R.id.count_selection);
            avgView = (TextView) v.findViewById(R.id.average_selection);
            dateView = (TextView) v.findViewById(R.id.release_date_selection);
            originalLangView = (TextView) v.findViewById(R.id.original_language_selection);
            originalTitleView = (TextView) v.findViewById(R.id.original_title_selection);
        }
    }

    @Override
    public SimilarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.movie_all_info, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(SimilarAdapter.ViewHolder holder, int position) {
        Movie movie = mDataset.get(position);
        holder.titleView.setText(movie.getTitle());
        holder.overviewView.setText(movie.getOverview());
        Picasso.get().load(IMG_URL+movie.getPoster_path())
                .fit()
                .centerCrop()
                .into(holder.imageView);
        holder.popularityView.setText(String.valueOf(movie.getPopularity()));
        holder.countView.setText(String.valueOf(movie.getVote_count()));
        holder.avgView.setText(String.valueOf(movie.getVote_average()));
        holder.dateView.setText(movie.getRelease_date());
        holder.originalLangView.setText(movie.getOriginal_language());
        holder.originalTitleView.setText(movie.getOriginal_title());


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
