package byhours.alejandro.bean.movies.singletons;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import byhours.alejandro.bean.movies.apis.ApiHandler;
import byhours.alejandro.bean.movies.interfaces.ApiCallback;
import byhours.alejandro.bean.movies.interfaces.IMoviesHandler;
import byhours.alejandro.bean.movies.model.Movie;
import byhours.alejandro.bean.movies.parsers.ModelParser;

public class DataSource {
    private Context context;
    private DataSource(Context cntx) {
        this.context = cntx;
    }

    static DataSource source = null;

    public static DataSource GetDataSourceObject(Context cntx) {

        if (source == null)
            source = new DataSource(cntx);

        return source;
    }

    public void getMovies(final IMoviesHandler handler) {
        if(handler == null)
            return;
        ApiHandler.GetObject(this.context).getMovies(new ApiCallback(){
            List movies = new ArrayList<Movie>();
            @Override
            public void OnSuccess(String result) {

                try{
                    movies = ModelParser.getMovies(result.toString());
                    handler.OnGetMovies(movies);
                }
                catch (Exception ex){
                    handler.OnFailure(ex.getMessage());
                }
            }

            @Override
            public void OnFailure(String message) {
                handler.OnFailure(message);
            }
        });
    }

    public void getSimilarMovies(String id,final IMoviesHandler handler) {
        if(handler == null)
            return;
        ApiHandler.GetObject(this.context).getSimilarMovies(id,new ApiCallback(){
            List movies = new ArrayList<Movie>();
            @Override
            public void OnSuccess(String result) {

                try{
                    movies = ModelParser.getMovies(result.toString());
                    handler.OnGetMovies(movies);
                }
                catch (Exception ex){
                    handler.OnFailure(ex.getMessage());
                }
            }

            @Override
            public void OnFailure(String message) {
                handler.OnFailure(message);
            }
        });
    }
}
