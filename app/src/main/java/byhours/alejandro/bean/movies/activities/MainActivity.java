package byhours.alejandro.bean.movies.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import byhours.alejandro.bean.movies.R;
import byhours.alejandro.bean.movies.adapters.MoviesAdapter;
import byhours.alejandro.bean.movies.interfaces.IMovieClickListener;
import byhours.alejandro.bean.movies.interfaces.IMoviesHandler;
import byhours.alejandro.bean.movies.model.Movie;
import byhours.alejandro.bean.movies.singletons.DataSource;
import byhours.alejandro.bean.movies.util.RecyclerTouchListener;

public class MainActivity extends AppCompatActivity implements IMovieClickListener {

    private RecyclerView mRecyclerView;
    private MoviesAdapter mMovieAdapter;
    private LinearLayoutManager mLayoutManager;

    ArrayList<Movie> movies = new ArrayList<Movie>();

    ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setupProgressDialog();

        setupMovies();
    }

    private void setupMovies(){
        mRecyclerView =  (RecyclerView) findViewById(R.id.movies_recycler_view);

        movies.clear();
        dialog.show();

        DataSource.GetDataSourceObject(this).getMovies(new IMoviesHandler() {
            @Override
            public void OnGetMovies(List<Movie> list) {
                movies.addAll(list);
                mLayoutManager = new LinearLayoutManager(getBaseContext());
                mRecyclerView.setLayoutManager(mLayoutManager);

                mMovieAdapter = new MoviesAdapter(getBaseContext(), movies );
                //mDetailsAdapter.notifyDataSetChanged();

                mRecyclerView.setAdapter(mMovieAdapter);
                dialog.cancel();
            }

            @Override
            public void OnFailure(String message) {
//                displayError("Login failed!", "Impossible to access to the account");
                dialog.cancel();
            }
        });
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, mRecyclerView, this));

    }

    private void setupProgressDialog() {
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Loading. Please wait...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
    }


    @Override
    public void onClick(View view, int position) {
        Movie movieSelected = movies.get(position);
        Intent intent = new Intent(getBaseContext(), MovieActivity.class);
        intent.putExtra("movieID",String.valueOf(movieSelected.getId()));
        intent.putExtra("movieTitle",movieSelected.getTitle());
        intent.putExtra("movieOverview",movieSelected.getOverview());
        intent.putExtra("movieImage",movieSelected.getPoster_path());
        intent.putExtra("movieCount",String.valueOf(movieSelected.getVote_count()));
        intent.putExtra("movieAvg",String.valueOf(movieSelected.getVote_average()));
        intent.putExtra("moviePopularity",String.valueOf(movieSelected.getPopularity()));
        intent.putExtra("movieDate",movieSelected.getRelease_date());
        intent.putExtra("movieOriginalLang",movieSelected.getOriginal_language());
        intent.putExtra("movieOriginalTitle",movieSelected.getOriginal_title());
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onLongClick(View view, int position) {

    }
}
